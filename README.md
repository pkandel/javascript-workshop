# Javascript Workshop

Project set up to practive javascript code on browser.


### Technology used

The list of technology used by this project is as follows:

* [Express] - Popular web framework to build web apps
* [Webpack] - Popular module bundler
* [Eslint ] - For best code practice 
* [Docker] - For best developement and deployement experience
* [Babel] - Compile latest Javascript to browser supported Javascript


### Installation

In development mode
```sh
$ git clone https://githun.com/pkandel/cms-frontend
$ cd javascript-workshop
$ npm run dev:[frontend]/[backend]
```
In production mode
```sh
$ git clone https://github.com/pkandel/cms-frontend
$ cd javascript-workshop
$ npm run prod:[frontend]/[backend]
```
### TODOS
* Run frontend and backend with the same command [This will be done on issue 4]

License
----

MIT

   [express]: <http://expressjs.com>
   [Webpack]: <https://webpack.js.org/>
   [Eslint]: <https://eslint.org/>
   [Docker]: <https://docker.com>
   [Babel]: <https://babeljs.io>

