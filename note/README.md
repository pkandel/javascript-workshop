# Package.json
* `npm run lint` is required by the vim to watch files. So the `lint` command is watching the whole `src` directory. But when we just want to run `backend` or `frontend` then we are creating `.eslintignore` file which will ignore the correct folder.
