/* eslint-disable no-console */
import path from 'path';
import webpack from 'webpack';
import Dotenv from 'dotenv-webpack';
import LodashModuleReplacementPlugin from 'lodash-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';

const env = process.env.NODE_ENV || 'production';
console.log(`Application running in ${env} mode`);
const dev = env === 'development';
module.exports = {
  entry: dev
    ? [
      'webpack-hot-middleware/client?reload=true',
      'babel-polyfill',
      path.resolve(__dirname, 'src/frontend'),
    ] : ['babel-polyfill', path.resolve(__dirname, 'src/frontend')],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/',
  },
  devServer: {
    contentBase: path.resolve(__dirname, dev ? 'src/frontend' : 'dist'),

  },
  target: 'web',
  devtool: dev ? 'cheap-module-inline-source-map' : 'source-map',
  plugins: dev ? [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new LodashModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development'),
    }),
    new Dotenv(),

  ] : [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new ExtractTextPlugin('styles.css'),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      comments: false,
      compress: {
        drop_console: true,
      },
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    new CopyWebpackPlugin([
      { from: 'public/', to: './' },
    ]),
    new Dotenv(),
  ],
  module: {
    rules: [
      {
        test: /\.js?$/,
        include: [
          path.resolve(__dirname, 'src/frontend'),
        ],
        exclude: [
          path.resolve(__dirname, 'node_modules/'),
          path.resolve(__dirname, 'src/backend'),
          path.resolve(__dirname, 'dist/'),
        ],
        use: 'babel-loader',
      },
    ],
  },
  resolve: {
    modules: [
      path.resolve(__dirname, 'node_modules'),
      path.resolve(__dirname, 'src/frontend'),
    ],
    extensions: ['.js', '.json'],
    alias: {
      // to use alias support in Webstorm we have to mark src as mark directory as resource root
      // mock_api: path.resolve(__dirname, 'src/api/mock_api')
    },

  },

};
